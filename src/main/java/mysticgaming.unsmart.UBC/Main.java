package mysticgaming.unsmart.UBC;

/*
  UltimateBungeeChat: An advanced bungee chat plugin
  If you have decompiled this the source code is at https://bitbucket.org/unsmart/ultimatebungeechat/src make sure to use that for the most up to date version!
 */

import java.io.*;
import java.util.List;


import com.google.common.io.ByteStreams;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

/**
 * @author Unsmart from play.mysticgaming.us
 */

public class Main extends Plugin {

    /*
    Set up instance of Main(this)
     */
    private static Main instance;
    public static Main getInstance() {
        return instance;
    }


    @Override
    public void onEnable() {
        boolean error = false;
        instance = this;

        /*
        Create config if needed...
         */
        if (!getDataFolder().exists()) {
            //noinspection ResultOfMethodCallIgnored
            getDataFolder().mkdir();
        }

        File file = new File(getDataFolder(), "config.yml");

        if (!file.exists()) {
            getLogger().info("Config.yml not found, creating!");
            try {
                //noinspection ResultOfMethodCallIgnored
                file.createNewFile();
                try (InputStream is = getResourceAsStream("config.yml");
                     OutputStream os = new FileOutputStream(file)) {
                    ByteStreams.copy(is, os);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }else {
            getLogger().info("Config.yml found, loading!");
        }
        /*
        Load config...
         */

        try {
            loadConfig();
        } catch (IOException e) {
            e.printStackTrace();
            error = true;
        }

        /*
        Resister commands...
         */
        try {
            getProxy().getPluginManager().registerCommand(this, new ClearChat());
            getProxy().getPluginManager().registerCommand(this, new ChatLock());
            getProxy().getPluginManager().registerCommand(this, new ChatUnlock());
        } catch (Error e) {
            getLogger().severe("Plugin couldn't load commands.");
            error = true;
        }

        /*
        Register listeners...
         */
        try {
            getProxy().getPluginManager().registerListener(this, new ChatListener());
        } catch (Error e){
            getLogger().severe("Plugin couldn't load listener.");
            error = true;
        }

        /*
        Check if error happened...
         */
        if (!error) {
            getLogger().info("Has successfully been enabled with no errors! (v " + getDescription().getVersion() + ")");
        }
        else{
            getLogger().severe("Has not loaded properly and will not work! Please report any errors in console to the dev on Spigot! (v " + getDescription().getVersion() + ")");
            this.onDisable();
        }


    }
    /*
    When plugin gets disabled
     */
    public void onDisable(){
        getLogger().info("Has been disabled thanks for using my plugin! ~ Unsmart (dev)");
    }

    /*
    Load config method
     */
    private Configuration config;
    public void loadConfig() throws IOException {
        File file = new File(getDataFolder(), "config.yml");
        if(!getDataFolder().exists())
            Log("No folder found?????");
        if(!file.exists()){
            Log("No config found????");
        }else{
            config = ConfigurationProvider.getProvider(YamlConfiguration.class).load(new File(getDataFolder(), "config.yml"));
            this.getInfo();
        }

    }

    /*
    Get config info
     */
    public static List<String> BlockedWords;
    private void getInfo(){
        BlockedWords = config.getStringList("BlockedWords");
    }

    /*
    Log a message
     */
    public static void Log(String message){
        System.out.println("[UBC] " + message);
    }
}
