package mysticgaming.unsmart.UBC;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

class ChatLock extends Command{
    public ChatLock() {
        super("chatlock", "ubc.chatlock", "cl");
    }

    public static boolean locked = false;

    @Override
    public void execute(CommandSender sender, String[] args) {
        if((sender instanceof ProxiedPlayer)){
            /*
            Lock the chat if unlocked...
             */
            if(!locked){
                if (args.length == 0) {
                    locked = true;
                    ProxyServer.getInstance().broadcast(new TextComponent("Chat has been locked by " + ((ProxiedPlayer) sender).getDisplayName() + "."));
                }else if(args.length == 1){
                    if(args[0].equals("-s")){
                        locked = true;
                        ProxyServer.getInstance().broadcast(new TextComponent("Chat has been locked."));
                    }else //noinspection StatementWithEmptyBody
                        if(args[0].equals("global")){

                    }
                }else //noinspection StatementWithEmptyBody
                    if(args[0].equalsIgnoreCase("-s") && args[1].equalsIgnoreCase("global") || args[1].equalsIgnoreCase("-s") && args[0].equalsIgnoreCase("global")){

                }
            }
            /*
            Chat not locked...
             */
            else{
                sender.sendMessage(new TextComponent("Chat is already locked!"));
            }

        }
        /*
        Console sent command
         */
        else{
            Main.Log("You must be a player to do this.");
        }
    }
}
