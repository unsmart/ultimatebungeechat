package mysticgaming.unsmart.UBC;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

class ChatUnlock extends Command {
    public ChatUnlock() {
        super("chatunlock", "ubc.chatunlock", "cu");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        /*
        Unlock chat if locked...
         */
        if ((sender instanceof ProxiedPlayer)) {
            if(ChatLock.locked){
                if (args.length == 0) {
                    ChatLock.locked = false;
                    ProxyServer.getInstance().broadcast(new TextComponent("Chat has been unlocked by " + ((ProxiedPlayer) sender).getDisplayName() + "."));
                } else if (args.length == 1) {
                    if (args[0].equals("-s")) {
                        ChatLock.locked = false;
                        ProxyServer.getInstance().broadcast(new TextComponent("Chat has been unlocked."));
                    } else if (args[0].equals("global")) {
                        sender.sendMessage(new TextComponent("This hasn't been setup yet sorry!"));
                    }
                }
            }else{
                sender.sendMessage(new TextComponent("Chat is already unlocked! (/cl to lock chat)"));
            }

        } else {
            Main.Log("You must be a player to do this.");
        }
    }
}
