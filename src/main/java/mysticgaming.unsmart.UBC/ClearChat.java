package mysticgaming.unsmart.UBC;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

class ClearChat extends Command {
    public ClearChat() {
        super("clearchat", "ubc.clearchat", "cc");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if((sender instanceof ProxiedPlayer)){
            if (args.length == 0) {
                String server = ((ProxiedPlayer) sender).getServer().getInfo().getName();
                for (ProxiedPlayer p : ProxyServer.getInstance().getServerInfo(server).getPlayers()) {
                    if(!(p.hasPermission("ubc.bypass")))
                        for(int i = 0; i < 50; i++)
                            ProxyServer.getInstance().broadcast(new TextComponent(""));
                    p.sendMessage(new TextComponent("Chat was cleared by: " + ((ProxiedPlayer) sender).getDisplayName()));
                }
            }else if(args.length == 1 && args[0].equals("-s")){
                String server = ((ProxiedPlayer) sender).getServer().getInfo().getName();
                for (ProxiedPlayer p : ProxyServer.getInstance().getServerInfo(server).getPlayers()) {
                    if(!(p.hasPermission("ubc.bypass")))
                        for(int i = 0; i < 50; i++)
                            p.sendMessage(new TextComponent(""));
                    p.sendMessage(new TextComponent("Chat has been cleared!"));
                }
            }
        }else{
            Main.Log("Sender is not a player");
        }

    }
}
