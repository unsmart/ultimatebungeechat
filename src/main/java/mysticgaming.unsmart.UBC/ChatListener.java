package mysticgaming.unsmart.UBC;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.io.IOException;
import java.util.List;

@SuppressWarnings("WeakerAccess")
public class ChatListener implements Listener {
    /*
    Check if message contains a blacklisted word
     */
    @EventHandler
    public void onChat(ChatEvent e){
        String msg = e.getMessage();
        String name = e.getSender().toString();
        ProxiedPlayer p = ProxyServer.getInstance().getPlayer(name);

        /*
        Reload config through command
         */
        if(msg.equalsIgnoreCase("/ubc reload") || msg.equalsIgnoreCase("/ubc rl")){
            if(p.hasPermission("ubc.reload")){
                try {
                    p.sendMessage(new TextComponent("Config attempting to reload..."));
                    Main.getInstance().loadConfig();
                    p.sendMessage(new TextComponent("Config was reloaded successfully!"));
                    e.setCancelled(true);
                } catch (IOException error) {
                    error.printStackTrace();
                    p.sendMessage(new TextComponent("Config failed to reload... Please check the console to find errors that ay be on your part (ex. tabs in config)"));
                }
            }
        }

        /*
        Broadcast command
         */
        else if(msg.startsWith("/ubc bc")){
            if(p.hasPermission("ubc.broadcast")){
                msg = ChatColor.translateAlternateColorCodes('&', msg);
                msg = msg.replace("/ubc bc", "");
                String bcastPrefix = "&6[Bcast]";
                bcastPrefix = ChatColor.translateAlternateColorCodes('&', bcastPrefix);
                ProxyServer.getInstance().broadcast(new TextComponent(bcastPrefix + " " + msg));
                e.setCancelled(true);
            }
        }

        /*
        Chat locked?
         */
        else if(ChatLock.locked){
            String locked = "Chat has been locked please wait for chat to be unlocked by a staff member!";
            /*
            Cancel event if bypass perm isn't given to player (unless a command not specified to be blocked)
             */
            if(!p.hasPermission("ubc.bypass")){
                if(msg.startsWith("/")){
                    if(msg.startsWith("/me")){
                        eventCancel(e, p, locked);
                    }else if(msg.startsWith("/minecraft:")){
                        eventCancel(e, p, locked);
                    }else if(msg.startsWith("/bukkit:")){
                        eventCancel(e, p, locked);
                    }else{
                        e.setCancelled(false);
                    }
                }else{
                    eventCancel(e, p, locked);
                }
            }
        }

        /*
        Chat not locked contains blacklisted word?
         */
        else if(!ChatLock.locked){
            String blacklisted = "A word in your message has been blacklisted please don't use blacklisted words!";
            msg = msg.replaceAll("\\s+","");
            List<String> bw = Main.BlockedWords;
            for (String aBw : bw) {
                if (msg.toLowerCase().contains(aBw.toLowerCase())) {
                    eventCancel(e, ProxyServer.getInstance().getPlayer(e.getSender().toString()), blacklisted);
                }
            }
        }

        /*
        Chat message is not getting blocked
         */
        else{
            //Add stuff to message? (you can do this yourself if you would like... I probably wont edit messages)
            e.setMessage(msg);
        }
    }
    /*
    Cancel the event
     */
    private void eventCancel(ChatEvent e, ProxiedPlayer p, String msg){
        e.setCancelled(true);
        p.sendMessage(new TextComponent(msg));
    }
}
